import Vue from 'vue'
import App from './comp/App.vue'
import store from './store/'

window.Vue = Vue;

new Vue({
  el: '#app',
  render: h => h(App),
  store
})
