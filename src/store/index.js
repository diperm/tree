import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    idd: 0,
    name: '',
    treeData: {
      names:['home','tree','first','second','leaf','leaf2','leaf3','leaf4'],
      name: 'My Tree',
      id:'1',
      children: [
        { name: 'hello', id:'2' },
        { name: 'wat', id:'3' },
        {
          name: 'child folder',
          id:'4',
          children: [
            {
              name: 'child folder',
              id: '5'
            },
            {
              id:'6'
            }
          ]
        }
      ]
    }
  },
  mutations: {
    getid(state,data) {
      state.idd=data.id;
      state.name=data.name;
    },
    save(state,data) {
      state.treeData.names[data.id]=data.name;
      state.treeData.names.push(
        'new stuff'
      );
      state.name='';
    },
    remove(state,data) {
      state.treeData.names[data.id]='';
      state.treeData.names.push(
        'new stuff'
      );
      state.name='';
    }
  }
});

export default store
